'use strict';

let mongoose = require('mongoose');
let Task = mongoose.model('Tasks');

exports.getAll = function (req, res) {
    Task.find({}, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.create = function (req, res) {
    var newTask = new Task(req.body);
    newTask.save(function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.get = function (req, res) {
    Task.findById(req.params.id, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};