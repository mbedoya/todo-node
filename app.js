let express = require('express');
let app = express()
let port = process.env.PORT || 3000;
let bodyParser = require('body-parser');
let routes = require("./routes/task.routes");
let router = express.Router();
let repository = require("./repository/mongo.repository")

repository();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
routes(router);
app.use("/api", router);

app.listen(port, () => console.log("Server Started."));