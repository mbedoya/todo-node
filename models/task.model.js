'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var taskSchema = new Schema({
  name: {
    type: String,
    required: 'Please enter the name of the task'
  },
  createdDate: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['Pending', 'OnGoing', 'Completed']
    }],
    default: ['Pending']
  }
});

module.exports = mongoose.model('Tasks', taskSchema);