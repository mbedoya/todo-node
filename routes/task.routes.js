'use strict';
module.exports = function (router) {
    let taskController = require("../controllers/task.controller");

    router.use(function (req, res, next) {
        console.log('Something is happening.');
        next();
    });

    router.get('/', function (req, res) {
        res.json({ message: "hooray! welcome to our api!" });
    });

    router.route("/tasks")
        .get(taskController.getAll)
        .post(taskController.create);

    router.route("/tasks/:id")
        .get(taskController.get);
};