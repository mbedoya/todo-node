"use strict";

let mongoose = require('mongoose');
require('../models/task.model');

module.exports = function () {

    mongoose.Promise = global.Promise;
    //mongoose.connect("mongodb://localhost/test");
    mongoose.connect("mongodb://task_user:12345@ds125588.mlab.com:25588/tests");
}